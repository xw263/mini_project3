# CDK TypeScript S3 Bucket

## Purpose
The goal of this project is to create infrastructure as code for an S3 bucket. This is a starting point to help us deploy AWS services effectively without having to use the AWS console.

## AWS CodeCatelyst 

### Codecatalyst S3 Bucket
![Screenshot_2024-02-10_at_5.24.40_PM](/uploads/fda5c1a9d41f778b41c547fadeaadf61/Screenshot_2024-02-10_at_5.24.40_PM.png)

### Bucket Version
![Screenshot_2024-02-10_at_5.32.08_PM](/uploads/cc603de04c33f4c1c0e5df2f83f72be1/Screenshot_2024-02-10_at_5.32.08_PM.png)

### Encryption
![Screenshot_2024-02-10_at_5.34.17_PM](/uploads/2a6d35b26b4b6254228a81eac160bc96/Screenshot_2024-02-10_at_5.34.17_PM.png)

## Steps
1. Have access to Codecatalyst (free is fine).
2. Connect a builder ID (needed for AWS Codewhisperer and Codecatalyst).
3. Create an empty dev environment.
4. Check if CDK is installed via cdk --version.
5. Now create an IAM user to use with Codecatalyst to deploy the S3 bucket.
6. Go to IAM, add a user, then add IAMFullAccess and AmazonS3FullAccess policies.
7. After finishing that, add an inline policy, then type CloudFormation and give access to all the commands.
8. After finishing that, add an inline policy, then type Systems Manager and give access to all the commands.
9. Create an access key and ID, save somewhere safe.
10. Type aws configure in the dev environment.
11. Fill out the AWS access key fields and leave the rest blank (note: you can fill the region but that may cause problems later on).
12. Create a project by first making a directory and then cd into it, then type cdk init app --language=typescript.
13. You now have a CDK template project, make your bucket or whatever you desire.
14. After you're done making your project, check it works properly with npm run build.
15. Create the CloudFormation template by typing cdk synth.
16. Deploy your CloudFormation template by typing cdk deploy.
17. If it fails to deploy, you need to attach a role to let it deploy.
18. First, create a JSON file, then copy zombie.json.
19. Find the error and identify the error role/copy what comes after this.
20. Type export ZOMBIE_ROLE=the_role_name_in_the_error_message.
21. Then create it aws iam create-role --role-name=$ZOMBIE_ROLE --assume-role-policy-document file://zombie.json.
22. Then attach it aws iam attach-role-policy --role-name $ZOMBIE_ROLE --policy-arn arn:aws:iam::aws:policy/AdministratorAccess.
23. Then try to deploy again cdk deploy.
24. Go to the AWS console and type in S3 to see your bucket live.


## AWS CodeWhisper
I have used Use CodeWhisper to create a configuration file describes the infrastructure components. This could include details such as the types and configurations of virtual machines, databases, storage, networking, security settings, etc.

## References
https://gitlab.com/jeremymtan/jeremytan_ids721_week3
